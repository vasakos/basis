# Basis

## About Basis

Basis is a repository of packages for VasakOS (Arch).

## How to "install" ts-arch-repo

Adding a third-party repository (like this one) is easy.  Just add the following lines to the end of /etc/pacman.conf :

```cfg
[basis]
SigLevel = Optional DatabaseOptional
Server = https://gitlab.com/vasakos/$repo/-/raw/main/$arch
```cfg

Then, sync the repositories and update your system with:
`sudo pacman -Syyu`
